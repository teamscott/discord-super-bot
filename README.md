# Discord Super Bot #

An example [Discord](https://discordapp.com/) bot for accessing the RocketLeague API.

### What is this repository for? ###

* Explore the RocketLeague API and what it has to offer.
* Explore the Discord bot API.
* Work in .NET Core.
