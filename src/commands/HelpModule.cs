using Discord;
using Discord.Commands;
using Discord.WebSocket;

using System;
using System.Threading.Tasks;

namespace DiscordSuperBot.Commands
{
    [Group("help"), Summary("Returns help information.")]
    public class HelpModule : ModuleBase
    {
        
    }
}