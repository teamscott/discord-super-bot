using Discord;
using Discord.Commands;
using Discord.WebSocket;

using System;
using System.Linq;
using System.Threading.Tasks;
using DiscordSuperBot.Services;
using DiscordSuperBot.Models;

namespace DiscordSuperBot.Commands
{
    [Group("group")]
    public class PlayerGroupModule : ModuleBase
    {
        private IDatabaseService databaseService;

        public PlayerGroupModule(IDatabaseService databaseService)
        {
            this.databaseService = databaseService;
        }

        [Command("add"), Summary("Adds a group.")]
        public async Task Add([Summary("The name of the player group to add.")] string groupName)
        {
            // add to the database if the group name isn't already there
            var groups = databaseService.GetGroups();
            if (groups.Exists(g => g.Name == groupName))
            {
                await Context.Channel.SendMessageAsync($"Group already exists: {groupName}");
            }
            else
            {
                groups.Add(new Group(groupName));
                await Context.Channel.SendMessageAsync($"Added group: {groupName}");
            }
        }

        [Command("list"), Summary("Lists all the groups.")]
        public async Task List()
        {
            var groups = databaseService.GetGroups();
            await Context.Channel.SendMessageAsync($"Currently registered groups:\n\n{String.Join("\n", groups.Select(g => g.Name))}");
        }
    }

    public class PlayerGroupHelpModule : HelpModule
    {
        [Command("group"), Summary("Displays help for math command.")]
        public async Task GroupHelp()
        {
            var helpString = @"
                Available commands:
                group add {groupName}
            ";
            await ReplyAsync(helpString);
        }
    }
}