using Discord;
using Discord.Commands;
using Discord.WebSocket;

using System;
using System.Linq;
using System.Threading.Tasks;
using DiscordSuperBot.Services;
using DiscordSuperBot.Models;

namespace DiscordSuperBot.Commands
{
    [Group("player")]
    public class PlayerModule : ModuleBase
    {
        private IDatabaseService databaseService;

        public PlayerModule(IDatabaseService databaseService)
        {
            this.databaseService = databaseService;
        }

        [Command("add"), Summary("Adds a player to a group.")]
        public async Task Add([Summary("The name of the player group to add.")] string playerName, string groupName)
        {
            // add to the database if the group name isn't already there
            var groups = databaseService.GetGroups();
            var players = databaseService.GetGroupMembers();
            if (players.Exists(p => p.Name == playerName))
            {
                await Context.Channel.SendMessageAsync($"Player already exists: {playerName}");
            }
            else
            {
                groups.Add(new Group(groupName));
                await Context.Channel.SendMessageAsync($"Added group: {groupName}");
            }
        }

        [Command("list"), Summary("Lists all the groups.")]
        public async Task List()
        {
            var groups = databaseService.GetGroups();
            await Context.Channel.SendMessageAsync($"Currently registered groups:\n\n{String.Join("\n", groups.Select(g => g.Name))}");
        }
    }

    public class PlayerHelpModule : HelpModule
    {
        [Command("player"), Summary("")]
        public async Task PlayerHelp()
        {
            var helpString = @"
                Available commands:
                player add {playerName} {groupName}
            ";
            await ReplyAsync(helpString);
        }
    }
}