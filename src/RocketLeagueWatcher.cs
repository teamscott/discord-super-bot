using System;
using System.Collections.Generic;
using System.Threading;
using DiscordSuperBot.Services;

namespace DiscordSuperBot
{
    public class RocketLeagueWatcher
    {


        public static void Run()
        {
            Console.WriteLine("Starting Rocket League Watcher thread.");
            while (true)
            {
                Console.WriteLine("Polling RL API");
                RocketLeagueDataService.GetPlayerData(new List<string>() { "76561198026879534", "76561197993209463", "76561197972367442" });
                Thread.Sleep(30000);
            }
        }

    }
}