using System;
using System.Collections.Generic;

namespace DiscordSuperBot.Models
{
    public class Group
    {
        public Group(string name)
        {
            Name = name;
            Id = new Guid().ToString();
        }

        public string Name { get; set; }
        public string Id { get; set; }

        public List<string> MemberIds { get; set; }
    }
}