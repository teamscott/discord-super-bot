namespace DiscordSuperBot.Models
{
    public class GroupMember
    {
        public string Name { get; set; }
        public string Id { get; set; }

        public int Matches { get; set; }

        public string Tier { get; set; }

        public string Division { get; set; }

        public int Goals { get; set; }

        public int Saves { get; set; }
        public int Shots { get; set; }
        public int Assists { get; set; }

        public int MVPs { get; set; }
    }
}