using System.Collections.Generic;
using DiscordSuperBot.Models;

namespace DiscordSuperBot.Services
{
    public interface IDatabaseService 
    {
        //void AddGroup(string groupName);
        List<Group> GetGroups();

        List<GroupMember> GetGroupMembers();

        
    }
}