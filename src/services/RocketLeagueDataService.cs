using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
//using Microsoft.AspNet.WebApi.Client;


namespace DiscordSuperBot.Services
{
    public static class RocketLeagueDataService
    {
        private const string jsonPlayerEntry = "{{\"platformId\":\"1\", \"uniqueId\":\"{0}\"}},";
        private const string jsonPlayerBatchTemplate = "[{0}]";

        public static void GetPlayerData(List<string> playerIds)
        {

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://api.rocketleaguestats.com/v1/player/batch");
            var playerJsonStrings = "";

            foreach (string id in playerIds)
            {
                playerJsonStrings += string.Format(jsonPlayerEntry, id);
            }
            playerJsonStrings.TrimEnd(',');

            var json = string.Format(jsonPlayerBatchTemplate, playerJsonStrings);

            request.Content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var client = new HttpClient();
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("ZGISZ8ULJL8ATVJXEED84CWXM5DFYCZZ");
            HttpResponseMessage response = client.SendAsync(request).Result;

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("RL call succeeded.");
                var obj = JArray.Parse(response.Content.ToString());
                foreach (JObject jObj in (obj))
                {
                    Console.WriteLine($"Name: {jObj["displayName"]}");
                    Console.WriteLine($"Wins: {jObj["stats"]["wins"]}");
                    Console.WriteLine($"Wins: {jObj["stats"]["goals"]}");
                    Console.WriteLine($"Wins: {jObj["stats"]["shots"]}");
                    Console.WriteLine($"Wins: {jObj["stats"]["assists"]}\n");
                }
            }
            else
            {

            }
        }
    }
}