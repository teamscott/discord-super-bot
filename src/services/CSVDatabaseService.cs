using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvHelper;
using DiscordSuperBot.Models;

namespace DiscordSuperBot.Services
{
    public class CSVDatabaseService : IDatabaseService
    {

        //private const string GROUP_DATABASE_FILE_NAME = "database_groups.csv";
        //private const string GROUP_MEMBER_DATABASE_FILE_NAME = "database_groups.csv";
        public const string DATABASE_DIRECTORY = "database";
        public const string GROUP_DATABASE_FILE_NAME = DATABASE_DIRECTORY + "/group.database.csv";
        public const string GROUP_MEMBER_DATABASE_FILE_NAME = DATABASE_DIRECTORY + "/group-member.database.csv";

        private Object _lockObject = new Object();
        private List<Group> _inMemoryGroups = null;
        private List<GroupMember> _inMemoryGroupMembers = null;

        public CSVDatabaseService()
        {
            if (!Directory.Exists(DATABASE_DIRECTORY))  // if it doesn't exist, create
                Directory.CreateDirectory(DATABASE_DIRECTORY);

            // create the database file if it isn't already there
            using (TextWriter w1 = File.CreateText(GROUP_DATABASE_FILE_NAME))
            using (TextWriter w2 = File.CreateText(GROUP_MEMBER_DATABASE_FILE_NAME))
            {
                // do nothing
            }
        }

        public List<Group> GetGroups()
        {
            if (_inMemoryGroups == null)
                _inMemoryGroups = GetGroupsFromCSV();

            return _inMemoryGroups;
        }

        public List<GroupMember> GetGroupMembers()
        {
            if (_inMemoryGroupMembers == null)
                _inMemoryGroupMembers = GetGroupMembersFromCSV();

            return _inMemoryGroupMembers;
        }

        private List<Group> GetGroupsFromCSV()
        {
            try
            {
                using (TextReader reader = File.OpenText(GROUP_DATABASE_FILE_NAME))
                {
                    using (CsvReader csvReader = new CsvReader(reader))
                    {
                        return csvReader.GetRecords<Group>().ToList();
                    }
                }
            }
            catch (Exception e)
            {
                return new List<Group>();
            }
        }

        private List<GroupMember> GetGroupMembersFromCSV()
        {
            using (TextReader reader = File.OpenText(GROUP_MEMBER_DATABASE_FILE_NAME))
            {
                using (CsvReader csvReader = new CsvReader(reader))
                {
                    return csvReader.GetRecords<GroupMember>().ToList();
                }
            }
        }
    }
}