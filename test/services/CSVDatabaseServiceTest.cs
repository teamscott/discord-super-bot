using System;
using System.IO;
using DiscordSuperBot.Services;
using Xunit;

namespace DiscordSuperBot.Test.Services
{
    public class CSVDatabaseServiceTest : IDisposable
    {
        private CSVDatabaseService databaseService;
        private string groupFileName = CSVDatabaseService.GROUP_DATABASE_FILE_NAME;
        private string groupMemberFileName = CSVDatabaseService.GROUP_MEMBER_DATABASE_FILE_NAME;

        public CSVDatabaseServiceTest()
        {
            databaseService = new CSVDatabaseService();
        }

        public void Dispose()
        {
            File.Delete(groupFileName);
            File.Delete(groupMemberFileName);
        }

        [Fact]
        public void GetGroupsReturnsWithEmptyCSV()
        {
            var groups = databaseService.GetGroups();
        }

        /*
                [Fact]
                public void AddGroupWorksIfNoDatabaseFileExistsTest()
                {
                    // arrange
                    if (File.Exists(dbFileName))
                        File.Delete(dbFileName);

                    Assert.False(File.Exists(dbFileName));

                    // act
                    databaseService.AddGroup("testGroup");

                    // assert
                    Assert.True(File.Exists(dbFileName));
                }

                [Fact]
                public void AddGroupWorksIfNoDatabaseFileExistsTest2()
                {
                    // arrange
                    if (File.Exists(dbFileName))
                        File.Delete(dbFileName);

                    Assert.False(File.Exists(dbFileName));

                    // act
                    databaseService.AddGroup("testGroup");

                    // assert
                    Assert.True(File.Exists(dbFileName));
                }
        */
    }
}
